package BigInteger.homework1;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;

/**
 * Created with IntelliJ IDEA.
 * User: mtjikuzu
 * Date: 1/27/13
 * Time: 11:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class Homework1 {

          public static void main(String[] args){

              BigIntGenerator bigIntGenerator = new BigIntGenerator();
              BigInteger  value1 = BigInteger.ZERO;
              BigInteger value2 = BigInteger.ZERO;

              int startingDigitsBase  = 200000;

              // Code Note: Incrementing By 1000 digits
              for (int i = 1; i <= 10; i++){
                 value1 = bigIntGenerator.getRandomNumber(startingDigitsBase);
                 value2 = bigIntGenerator.getRandomNumber(startingDigitsBase + 1);
                 long time1 = System.currentTimeMillis();
                  BigInteger result = value1.multiply(value2);
                 long time2 = System.currentTimeMillis();
                 try {

                     FileWriter write = new FileWriter("results.txt", true);
                     PrintWriter print_line = new PrintWriter(write);
                     print_line.println(startingDigitsBase + "\t " + (time2 - time1) + " milliseconds");
                     print_line.close();
                 }catch (IOException e)
                 {
                     e.printStackTrace();
                 }

                startingDigitsBase += 100000;

              }

          }
}
