package BigInteger.homework1;

import java.math.BigInteger;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: mtjikuzu
 * Date: 1/27/13
 * Time: 1:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class BigIntGenerator {

    public BigInteger getRandomNumber(final int digCount)
    {
        return getRandomNumber(digCount, new Random());
    }

    public BigInteger getRandomNumber(final int digCount, Random rnd)
    {
        final char[] ch = new char[digCount];
        for (int i = 0; i < digCount; i++)
        {
            ch[i] = (char) ('0' + (i == 0 ? rnd.nextInt(9) + 1 : rnd.nextInt(10)));
        }

        return  new BigInteger(new String(ch));
    }
}

/***************************************************************************************
 *    Original Author:
 *    Title: Generating a random n digit integer in Java using the BigInteger class
 *    Author: Sean Patrick Floyd
 *    Date: September 14, 2010
 *    Code version: 0
 *    Availability: http://stackoverflow.com/questions/3709521/how-do-i-generate-a-random-n-digit-integer-in-java-using-the-biginteger-class
 *
 ***************************************************************************************/

