package BigInteger.homework2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;

/**
 * Created with IntelliJ IDEA.
 * User: mtjikuzu
 * Date: 1/31/13
 * Time: 10:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class LinearFibonnaci {

    public BigInteger Fibonacci(int n)
    {
        BigInteger f1 = BigInteger.ZERO;
        BigInteger f2 = BigInteger.ONE;
        BigInteger fn = BigInteger.ZERO;
        long starttime = System.currentTimeMillis();
        for ( int i = 2; i < n; i++ )
        {
            fn = f1.add(f2);
            f1 = f2;
            f2 = fn;
        }
        long endtime = System.currentTimeMillis();
        try {

            FileWriter write = new FileWriter("linear.txt", true);
            PrintWriter print_line = new PrintWriter(write);
            print_line.println(n + "\t " +  fn.toString().length() + "\t " + (endtime - starttime));
            print_line.close();
        }catch (IOException e)
        {
            e.printStackTrace();
        }
        return fn;
    }
}
