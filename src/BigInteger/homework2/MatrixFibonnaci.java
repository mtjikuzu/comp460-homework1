package BigInteger.homework2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;

/**
 * Created with IntelliJ IDEA.
 * User: mtjikuzu
 * Date: 1/31/13
 * Time: 10:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class MatrixFibonnaci {

    public BigInteger Fibonacci(int n)
    {
        BigInteger fib[][] = {{BigInteger.ONE, BigInteger.ONE}, {BigInteger.ONE, BigInteger.ZERO}};

        if (n == 0)
        {
            return BigInteger.ZERO;
        }
        long starttime = System.currentTimeMillis();
        power(fib, (n  - 1));
        long endtime = System.currentTimeMillis();
        try {

        FileWriter write = new FileWriter("matrix.txt", true);
        PrintWriter print_line = new PrintWriter(write);
        print_line.println(n + "\t " +  fib[0][0].toString().length() + "\t " + (endtime - starttime));
        print_line.close();
    }catch (IOException e)
    {
        e.printStackTrace();
    }
        return fib[0][0];
    }

    public void power(BigInteger[][] fib, int n) {
        if (n == 0 || n == 1)
            return;
        BigInteger matrix[][] = {{BigInteger.ONE, BigInteger.ONE}, {BigInteger.ONE, BigInteger.ZERO}};
        power(fib, n/2);
        multiply(fib, fib);
        if(n%2 != 0)
            multiply(fib, matrix);
    }

    public void multiply(BigInteger[][] fib, BigInteger[][] matrix) {
        BigInteger x =  (fib[0][0].multiply(matrix[0][0])).add((fib[0][1].multiply(matrix[1][0])));
        BigInteger y =  (fib[0][0].multiply(matrix[0][1])).add((fib[0][1].multiply(matrix[1][1])));
        BigInteger z =  (fib[1][0].multiply(matrix[0][0])).add((fib[1][1].multiply(matrix[1][0])));
        BigInteger w =  (fib[1][0].multiply(matrix[0][1])).add((fib[1][1].multiply(matrix[1][1])));

        fib[0][0] = x;
        fib[0][1] = y;
        fib[1][0] = z;
        fib[1][1] = w;
    }
}


/***************************************************************************************
 *    Original Author:
 *    Title: Generating a random n digit integer in Java using the BigInteger class
 *    Author: GeeksforGeeks
 *    Date: March 6, 2011
 *    Code version: 0
 *    Availability: http://www.geeksforgeeks.org/program-for-nth-fibonacci-number/
 *
 ***************************************************************************************/
